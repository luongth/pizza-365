




  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
   const gORDER_COL = ["orderCode","kichCo","loaiPizza","idLoaiNuocUong","thanhTien","hoTen","soDienThoai","trangThai","chitiet"]
var gDataUser = [];
var gUserPut = []
var gId =[];
// js Pizza356
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  var gComboMenu = [];
  var gPizza =[];
  // các combo Pizza
  const gCOMBO_SMALL = "Small";
  const gCOMBO_MEDIUM = "Medium";
  const gCOMBO_LARGE = "Large";

  // các loại pizza
  const gPIZZA_TYPE_GA = "CHICKEN";
  const gPIZZA_TYPE_HAI_SAN = "SEAFOOD";
  const gPIZZA_TYPE_BACON = "BACON";
  //
  var gCodeVoucher = []
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  $(document).ready(function(){
    onPageLoading();
    $("#user-table").on("click",".btn-detail",function() {
      var vOjbUserOrder = getDataUserFromButton(this)
      gId = vOjbUserOrder.id
         loadDataUserModal(vOjbUserOrder)
        $("#my-modal").modal("show");
    });
    //event click filter User
    $("#btn-fliter").click(function() {
      onBtnFilterUserClick()
    });
    //event click Confirmed
    $("#btn-confirm").click(function() {
      onBtnComfirmClick()
    });
    $("#btn-cancel").click(function() {
      onBtnCancleClick()
    });
    // js Pizza356
    //event click combo menu
    //btn Small
    $("#btn-small").click(function() {
      onBtnSmallSizeClick()
    });
    //btn medium
    $("#btn-medium").click(function() {
      onBtnMediumSizeClick()
    });
    //btn Large
    $("#btn-large").click(function() {
      onBtnLargeSizeClick()
    });
    //type Pizza
    //Pizza bacon
    $("#btn-bacon").click(function() {
      onBtnBaconClick()
    });
    //Pizza chicken
    $("#btn-chiken").click(function() {
      onBtnGaClick()
    });
    //Pizza seafood
    $("#btn-seafood").click(function() {
      onBtnHaiSanClick()
    })
    //create order
    $("#btn-send").click(function() {
      onBtnGuiDonClick()
    });
    // event btn comfim 
    $("#btn-confirm-ordercode").click(function(){
      onBtnComfimOrderCodeClick()
    });
    //event click huy bo
    $("#btn-cancel-ordercode").click(function() {
      $("#my-modal-create-Order").modal('hide');
    })
     //on reload 
     $("#modal-reset").click(function() {
      resetOnLoad()
     })
  })
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */  
    //onload
    function onPageLoading() {
        getApiOrderUser()
        loadDataTableUser(gDataUser)
        //Pizza365
        //onload Pizza 356
        getDataApiDrink()
    };
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //load data table
  function dataTable() {
    var vTable = $("#user-table").DataTable({
        columns:[
            {data:gORDER_COL[0]},
            {data:gORDER_COL[1]},
            {data:gORDER_COL[2]},
            {data:gORDER_COL[3]},
            {data:gORDER_COL[4]},
            {data:gORDER_COL[5]},
            {data:gORDER_COL[6]},
            {data:gORDER_COL[7]},
            {data:gORDER_COL[8]},
        ],
        columnDefs:[
              {targets:[8],
               className:"text-center",
               defaultContent:`<button class= 'btn btn-primary  btn-detail'> Chi tiết</i></button>`
            }                
          ]   
    })
    return vTable
  };
  //put api status orderrlist
  function putStatusOrderList(paramStatus) {
    var vArrayIndex = getArrayUserById(gId);
    const baseUrl = "http://203.171.20.210:8080/devcamp-pizza365/orders/"  + gId;
    $.ajax({
      url: baseUrl,
      async:false,
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(paramStatus),
      //B4: xử lý hiển thị
      success: function(res){
        console.log(res)
          gDataUser.splice(vArrayIndex,1, res);
          console.log("id User " + res.id)
      },
      error: function(ajaxContext){
          alert (ajaxContext.responseText);
      }
  });
 
  }
  // get api json Order User
  function getApiOrderUser() {
     var vBaseUrl = "http://203.171.20.210:8080/devcamp-pizza365/orders";
    $.ajax({
        url: vBaseUrl,
        async:false,
        type: "GET",
        success:function(data){
            //console.log(data)
            gDataUser = data;
            console.log(gDataUser)
        },
        error: function(ajaxContext){
            alert(ajaxContext.responseText)
        }
      })
    };
    //load data table
function loadDataTableUser(paramTeacherArray){
    var vDataTable = dataTable()
    vDataTable.clear();
    vDataTable.rows.add(paramTeacherArray);
    vDataTable.draw();
 };
  // this is get data userOrder by button 
function getDataUserFromButton(paramElement) {
    var vRowClick = $(paramElement).closest("tr"); // Xác định tr chứa nút bấm được click
    var vTable = $('#user-table').DataTable(); // tạo biện truy xuất đến datatable
    var vDataRow = vTable.row( vRowClick ).data(); // Lấy dữ liệu của hàng dữ liệu chứa nút bấm được click
    console.log(vDataRow)
    return vDataRow
  };
  //this is event click btn Comfim
  function onBtnComfirmClick() {
    debugger
    var vObjStatus = {
         trangThai : "confirmed"
    }
   
      putStatusOrderList(vObjStatus)
      alert("update data success!");
      // B3 load data table
      loadDataTableUser(gDataUser);
     $("#my-modal").modal("hide");
    
  }
  //this is event click btn cancle
  function onBtnCancleClick() {
    var vObjStatus = {
         trangThai : "cancle"
    }
    if(gId !=""){
      putStatusOrderList(vObjStatus)
      // B3 load data table
      loadDataTableUser(gDataUser);
     $("#my-modal").modal("hide");
    }
  };
  //this is event click btn Fliter user
  function onBtnFilterUserClick(){
    // khai báo đối tượng để chứa dữ liệu trên form
    var vFilterObj = {
      trangThai: "",
      loaiPizza: ""
    };
    // B1 thu thập dữ liệu
    getFilterFormData(vFilterObj);
    console.log(vFilterObj)
    // B2 xử lý lọc dữ liệu.
    var vUsersResult = filterUsers(vFilterObj);
    console.log(vUsersResult)
    // B3 Hiển thị
    loadDataTableUser(vUsersResult);
  };
   // hàm thu thập dữ liệu lọc trên form
   function getFilterFormData(paramFilterObj) {
    paramFilterObj.trangThai = $("#inp-status-order").val().trim();
    paramFilterObj.loaiPizza = $("#inp-pizza").val().trim();
  };
   // hàm thực hiện lọc users trong mảng, và trả về mảng users sau khi lọc thỏa mãn điều kiện lọc
   function filterUsers(paramFilterObj) {
    var vUsers = gDataUser.filter( function(paramDataUser) {
      return ( removeVietnameseTones(paramDataUser.trangThai.toLowerCase()).includes(removeVietnameseTones(paramFilterObj.trangThai.toLowerCase())) || paramFilterObj.trangThai == "")
        && ( removeVietnameseTones(paramDataUser.loaiPizza.toLowerCase()).includes(removeVietnameseTones(paramFilterObj.loaiPizza.toLowerCase())) || paramFilterObj.loaiPizza == "");
    } );
    return vUsers;
  };
  // get array by id
function getArrayUserById(paramTypeId) {
  "use strict";
  var vNewIndex = "";
  var vIndex = 0;
  var vIsTypeFound = false;
  while(!vIsTypeFound && vIndex < gDataUser.length) {
    if(gDataUser[vIndex].id === paramTypeId) {
      vIsTypeFound = true;
      vNewIndex = vIndex;
    }
    else {
      vIndex ++;
    }
  }
  return vNewIndex;
}
  // hàm xóa bỏ dấu tiếng Việt
  function removeVietnameseTones(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a"); 
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e"); 
    str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i"); 
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o"); 
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u"); 
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y"); 
    str = str.replace(/đ/g,"d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    // Some system encode vietnamese combining accent as individual utf-8 characters
    // Một vài bộ encode coi các dấu mũ, dấu chữ như một kí tự riêng biệt nên thêm hai dòng này
    str = str.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ""); // ̀ ́ ̃ ̉ ̣  huyền, sắc, ngã, hỏi, nặng
    str = str.replace(/\u02C6|\u0306|\u031B/g, ""); // ˆ ̆ ̛  Â, Ê, Ă, Ơ, Ư
    // Remove extra spaces
    // Bỏ các khoảng trắng liền nhau
    str = str.replace(/ + /g," ");
    str = str.trim();
    // Remove punctuations
    // Bỏ dấu câu, kí tự đặc biệt
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g," ");
    return str;
};
// on load data user in form modal 
function loadDataUserModal(paramDataUser) {
   $("#int-orderid").val(paramDataUser.id);
   $("#select-combo").val(paramDataUser.kichCo).change();
   $("#int-duongkinh").val(paramDataUser.duongKinh);
   $("#int-suonnuong").val(paramDataUser.suon);
   $("#select-drink").val(paramDataUser.idLoaiNuocUong);
   $("#int-numberdrink").val(paramDataUser.soLuongNuoc);
   $("#int-vuocher").val(paramDataUser.idVourcher);
   $("#select-pizza").val(paramDataUser.loaiPizza);
   $("#int-salad").val(paramDataUser.salad);
   $("#int-price").val(paramDataUser.thanhTien);
   $("#int-discount").val(paramDataUser.giamGia);
   $("#int-fullname").val(paramDataUser.hoTen);
   $("#int-email").val(paramDataUser.email);
   $("#int-numberphone").val(paramDataUser.soDienThoai);
   $("#int-address").val(paramDataUser.diaChi);
   $("#int-message").val(paramDataUser.loiNhan);
   $("#int-status").val(paramDataUser.trangThai);
   $("#int-datecreated").val(paramDataUser.ngayCapNhat);
   $("#int-updateday").val(paramDataUser.ngayTao);
};
// js Pizza356
  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
//get api data Drink
function getDataApiDrink() {
  $.ajax({
    url: "http://203.171.20.210:8080/devcamp-pizza365/drinks",
    async:false,
    type: "GET",
    success:function(data){
      loadSelectDrink(data)
      console.log(data)
      },
    error: function(ajaxContext){
        alert(ajaxContext.responseText)
    }
});
}
 //this is load index select drink Pizza365
 function loadSelectDrink(paramDataDrink) {
  var vSelectDrink = $("#select-drink")
  for(var i = 0; i <paramDataDrink.length ; i++){
   var vOption = $("<option>").text(paramDataDrink[i].tenNuocUong)
                              .val(paramDataDrink[i].maNuocUong)
   vOption.appendTo(vSelectDrink)
  }
 };
//this is index combo menu
function getComboSelected(
  paramMenuName,
  paramDuongKinhCM,
  paramSuongNuong,
  paramSaladGr,
  paramDrink,
  paramPriceVND
) {
  var vSelectedMenu = {
    menuName: paramMenuName, // S, M, L
    duongKinhCM: paramDuongKinhCM,
    suongNuong: paramSuongNuong,
    saladGr: paramSaladGr,
    drink: paramDrink,
    priceVND: paramPriceVND,
    displayInConsoleLog() {
      console.log("%cPLAN MENU COMBO - .........", "color:blue");
      console.log(this.menuName); //this = "đối tượng này"
      console.log("duongKinhCM: " + this.duongKinhCM);
      console.log("suongNuong: " + this.suongNuong);
      console.log("saladGr: " + this.saladGr);
      console.log("drink:" + this.drink);
      console.log("priceVND: " + this.priceVND);
    },
  };
  return vSelectedMenu;
};
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  // hàm được gọi khi bấm nút chọn kích cỡ S
  function onBtnSmallSizeClick() {
    "use strict";
    // chỉnh combo, combo được chọn là Small, đổi nút, và đánh dấu
    changeComboButtonColor(gCOMBO_SMALL);
    //tạo một đối tượng menu, được tham số hóa
      gComboMenu = getComboSelected("S", 20, 2, 200, 2, 150000);
    // gọi method hiển thị thông tin
    gComboMenu.displayInConsoleLog();
  }
  // hàm được gọi khi bấm nút chọn kích cỡ M
  function onBtnMediumSizeClick() {
    "use strict";
    // chỉnh combo, combo được chọn là Medium, đổi nút, và đánh dấu
    changeComboButtonColor(gCOMBO_MEDIUM);
    //tạo một đối tượng menu, được tham số hóa
        gComboMenu = getComboSelected("M", 25, 4, 300, 3, 200000);
    // gọi method hiển thị thông tin
    gComboMenu.displayInConsoleLog();
  }
  // hàm được gọi khi bấm nút chọn kích cỡ L
  function onBtnLargeSizeClick() {
    "use strict";
    // chỉnh combo, combo được chọn là Large, đổi nút, và đánh dấu
    changeComboButtonColor(gCOMBO_LARGE);
    //tạo một đối tượng menu, được tham số hóa
       gComboMenu= getComboSelected("L", 30, 8, 500, 4, 250000);
    // gọi method hiển thị thông tin
    gComboMenu.displayInConsoleLog();
  }
  // hàm được gọi khi bấm nút chọn loại pizza gà
  function onBtnGaClick() {
    "use strict";
    // chỉnh loại pizza, loại pizza được chọn là GÀ , đổi nút, và đánh dấu
    changeTypeButtonColor(gPIZZA_TYPE_GA);
    // ghi loại pizza đã chọn ra console
         gPizza = gPIZZA_TYPE_GA
    console.log("Loại pizza đã chọn: " + gPizza);
  }
  // hàm được gọi khi bấm nút chọn loại pizza Hải sản
  function onBtnHaiSanClick() {
    "use strict";
    // chỉnh loại pizza, loại pizza được chọn là Hải sản, đổi nút, và đánh dấu
             changeTypeButtonColor(gPIZZA_TYPE_HAI_SAN);
             gPizza = gPIZZA_TYPE_HAI_SAN
    // ghi loại pizza đã chọn ra console
    console.log("Loại pizza đã chọn: " + gPIZZA_TYPE_HAI_SAN);
  }
  // hàm được gọi khi bấm nút chọn loại pizza Hun khói
  function onBtnBaconClick() {
    "use strict";
    // chỉnh loại pizza, loại pizza được chọn là Bacon, đổi nút, và đánh dấu
    changeTypeButtonColor(gPIZZA_TYPE_BACON);
    gPizza = gPIZZA_TYPE_BACON
    // ghi loại pizza đã chọn ra console
    console.log("Loại pizza đã chọn: " + gPIZZA_TYPE_BACON);
  }
   //Hàm được gọi khi ấn nút Gửi đơn
   function onBtnGuiDonClick() {
    var vOrder = getDataOrderUser();
    var vCheck = checkValidatedForm(vOrder)
    if(vCheck == true){
      console.log(vOrder);
      showDataformModal(vOrder)
      $("#my-modal-create-Order").modal('show');
     
    }
    
  }
  //this is event click btn comfim
  function onBtnComfimOrderCodeClick() {
    var vOrder = getDataOrderUser();
    var vCheckDataInput = checkValidatedForm(vOrder)
    if(vCheckDataInput == true){
      postCreateOrder(vOrder);
      $("#my-modal-create-Order").modal('hide');
    }
  }
 /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
 //change color Btn combo menu
   function changeComboButtonColor(paramPlan) {
        if(paramPlan == gCOMBO_SMALL){
          $("#btn-small").addClass("btn-success").removeClass("btn-warning")
          $("#btn-medium").addClass("btn-warning")
          $("#btn-large").addClass("btn-warning ")
        }
        else if(paramPlan  == gCOMBO_MEDIUM){
          $("#btn-small").addClass("btn-warning ")
          $("#btn-medium").addClass("btn btn-success ").removeClass("btn-warning")
          $("#btn-large").addClass("btn btn-warning ")
        }
        else if(paramPlan == gCOMBO_LARGE){
          $("#btn-small").addClass("btn btn-warning ")
          $("#btn-medium").addClass("btn btn-warning ")
          $("#btn-large").addClass("btn btn-success ").removeClass("btn-warning")
        }
   };
   //change color btn Pizza
   function changeTypeButtonColor(paramType) {
    if(paramType == gPIZZA_TYPE_BACON){
      $("#btn-bacon").addClass("btn-success").removeClass("btn-warning")
      $("#btn-chiken").addClass("btn-warning")
      $("#btn-seafood").addClass("btn-warning")
    }
    else if(paramType  == gPIZZA_TYPE_GA){
      $("#btn-bacon").addClass("btn-warning")
      $("#btn-chiken").addClass("btn-success").removeClass("btn-warning")
      $("#btn-seafood").addClass("btn-warning")
    }
    else if(paramType == gPIZZA_TYPE_HAI_SAN){
      $("#btn-bacon").addClass("btn btn-warning")
      $("#btn-chiken").addClass("btn btn-warning")
      $("#btn-seafood").addClass("btn btn-success").removeClass("btn-warning")
    }
   };
// get Api voucher 
function getApiVoucher(paramVoucherId) {
  var vResponse = null
    if(paramVoucherId ==""){
       vResponse = null
    }
    else{
      $.ajax({
        url:  "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + paramVoucherId,
        async:false,
        type: "GET",
        success:function(data){
          loadSelectDrink(data)
          console.log(data)
          vResponse = data
          gCodeVoucher = data.maVoucher
          },
        error: function(ajaxContext){
            alert("Không có mã giảm giá")
        }
    });
    } 
    return vResponse
}
//validate data user input
 // output: dung/ sai (true/ false)
 function checkValidatedForm(paramOrder) { 
  if (paramOrder.menuCombo == "") {
    alert("Chọn combo pizza...");
    return false;
  }
  if (paramOrder.loaiPizza == "") {
    alert("Chọn loại pizza...");
    return false;
  }
  if (paramOrder.orderdrink =="chọn đồ uống") {
    alert("Chọn loại đồ uống...");
    return false;
  }
  if (paramOrder.hoVaTen == "") {
    alert("Nhập họ và tên");
    return false;
  }
  if (isEmail(paramOrder.email) == false) {
    return false;
  }

  if (paramOrder.dienThoai == "") {
    alert("Nhập vào số điện thoại...");
    return false;
  }
  if (paramOrder.diaChi === "") {
    alert("Địa chỉ không để trống...");
    return false;
  }
  return true;
}
  //hàm kiểm tra email validate email 
  function isEmail(paramEmail) {
    if (paramEmail < 3) {
      alert("Nhập email...");
      return false;
    }
    if (paramEmail.indexOf("@") === -1) {
      alert("Email phải có ký tự @");
      return false;
    }
    if (paramEmail.startsWith("@") === true) {
      alert("Email không bắt đầu bằng @");
      return false;
    }
    if (paramEmail.endsWith("@") === true) {
      alert("Email kết thúc bằng @");
      return false;
    }
    return true;
  }

//get data infor Order user
function getDataOrderUser() {
  var vName  = $("#int-name").val().trim();
  var vEmail = $("#int-email").val().trim();
  var vNumberPhone = $("#int-numberphone").val().trim();
  var vAddress = $("#int-address").val().trim();
  var vVoucher = $("#int-voucher").val().trim();
  var vMessage = $("#int-message").val().trim();
  var vOrderdrink =  $("#select-drink").val();
  var vPercent = 0;
  // goi ham tinh phan tram
  var vVoucherObj = getApiVoucher(vVoucher);
  if (vVoucher != ""  && vVoucherObj != null) {
    vPercent = vVoucherObj.phanTramGiamGia;
  }
  var vOrderInfo = {
    menuCombo: gComboMenu,
    loaiPizza: gPizza,
    hoVaTen: vName,
    email: vEmail,
    dienThoai: vNumberPhone,
    diaChi: vAddress,
    loiNhan: vMessage,
    voucher: vVoucher,
    orderdrink:vOrderdrink,
    phanTramGiamGia:function(){
      var vDiscount = 0 
      if(vPercent){
        vDiscount = vPercent
      }
      return vDiscount
      },
    priceAnnualVND: function () {
      var vTotal = this.menuCombo.priceVND
      if(vPercent){
        var vTotal = this.menuCombo.priceVND * (1 - vPercent / 100);
      }
      return vTotal;
    }
  };
  return vOrderInfo;
};
// create Order
function postCreateOrder(paramOrder) { 
  //base url
  const vBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";
  var vObjUser =  {
    kichCo: paramOrder.menuCombo.menuName ,
   duongKinh: paramOrder.menuCombo.duongKinhCM,
   suon: paramOrder.menuCombo.suongNuong,
   salad:paramOrder.menuCombo.saladGr,
   loaiPizza:  paramOrder.loaiPizza ,
   idVourcher:  paramOrder.voucher,
   idLoaiNuocUong: paramOrder.orderdrink,
   soLuongNuoc:  paramOrder.menuCombo.drink,
   hoTen: paramOrder.hoVaTen,
   thanhTien:paramOrder.priceAnnualVND(),
   email:paramOrder.email,
   soDienThoai:paramOrder.dienThoai,
   diaChi: paramOrder.diaChi,
   loiNhan: paramOrder.loiNhan
  };
  $.ajax({
    url:  vBASE_URL,
    type: 'POST',
    dataType: 'json', // added data type
    contentType: "application/json",
    data:JSON.stringify(vObjUser),
    success: function (responseObject) {
      //bước 4. xử lý hiện thị 
        alert("Post success full")
        showindexVoucherCodeModal(responseObject.orderCode)
     console.log(responseObject)
    },
    error: function (ajaxContext) {
      alert(ajaxContext.responseText);
    }
  });
};
// show data user modal
function showDataformModal(paramOrder){
  var vDiv = $("#select-index")
  $("#inp-fullname").val(paramOrder.hoVaTen);
  $("#inp-numberphone").val(paramOrder.dienThoai);
  $("#inp-address").val(paramOrder.diaChi);
  $("#inp-message").val(paramOrder.loiNhan);
  $("#inp-discount").val(gCodeVoucher) 
   $("#select-index").html(
  ` 
  <option selected>${"Xác nhận:"}${paramOrder.hoVaTen},${paramOrder.dienThoai},${paramOrder.diaChi}</option>
  <option value="1">${"Menu:"}${paramOrder.menuCombo.suongNuong},${"Suờn nướng"}${paramOrder.menuCombo.suongNuong},${"nước"}${paramOrder.menuCombo.drink} </option>
  <option value="2">${"Phải thanh toán:"}${paramOrder.priceAnnualVND()}${"vnd"}${"(giảm giá"}${paramOrder.phanTramGiamGia()}${"%)"}</option>
  <option value="3"></option>
  `
   )
};
//this is show index create voucher in modal
function showindexVoucherCodeModal(paramVoucherCode) {
   $("#inp-order-code").val(paramVoucherCode);
   $("#modal-order-code").modal("show")
};
// this is reset on load
function resetOnLoad() {
  location.reload();
  $("#my-modal-create-Order").modal('hide');
}